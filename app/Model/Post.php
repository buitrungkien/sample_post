<?php
  class Post extends AppModel{
     public $validate = array(
        'title' => array(
            'rule' => 'notEmpty',
            'rule' => array('minLength', '5'),
	    'message' => 'Title must have more than 5 chars'
        ),
        'body' => array(
            'rule' => 'notEmpty'
        )
    );
  }
?>
